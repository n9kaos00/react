import React from 'react';

export default class Form extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            description: ''
        };
    }

    handleSubmit = (event) => {
        this.props.add(this.state.title, this.state.description);
        this.setState({
            title: '',
            description: ''
        });

        event.preventDefault();
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render () {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label>Title</label>
                    <input name="title" value={this.state.title} onChange={this.handleChange} className="form-control"/>
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <textarea name="description" value={this.state.description} onChange={this.handleChange}  className="form-control"></textarea>
                </div>
                <button className="btn btn-info">Add</button>
            </form>
        )
    }
}
