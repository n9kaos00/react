import React from 'react';
import uuid from 'react-uuid';
import Row from './Row';
import Todo from './Todo';
import Form from './Form';

export default class List extends React.Component {
    constructor(props) {
        super(props);

        if (!localStorage.getItem("savedData")) {
            localStorage.setItem("savedData", JSON.stringify(new Array(new Todo('Testi1', 'Test description'))));
        }

        this.state = {
            todos: JSON.parse(localStorage.getItem("savedData"))
        };
    }

    addToList = (title, description) => {
        localStorage.setItem("savedData", JSON.stringify([...this.state.todos, new Todo(title, description)]));

        this.setState({
            todos: JSON.parse(localStorage.getItem("savedData"))
        });
    }

    removeFromList = (index) => {
        this.state.todos.splice(index, 1);
        localStorage.setItem("savedData", JSON.stringify(this.state.todos));

        this.setState({
            todos: JSON.parse(localStorage.getItem("savedData"))
        });
    }

    render() {
        return (
            <div className="container">
                    <h3>Todos with localStorage and Remove</h3>
                    <Form add={this.addToList} />
                    <table className="table table-primary mt-3">
                        <thead className="thead-dark">
                            <tr>
                                <th scope="col">Title</th>
                                <th scope="col">Description</th>
                                <th scope="col">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.todos.map((item, index) => (
                                <Row key={uuid} title={item.title} description={item.description} index={index} remove={this.removeFromList} />
                            ))}
                        </tbody>
                    </table>
            </div>
        )
    }
}