import React from 'react'

export default class Row extends React.Component {

    constructor(props) {
        super(props);
    }

    handleClick = () => {
        this.props.remove(this.props.index);
    }

    render() {
        return (
            <tr>
                <td>{this.props.title}</td>
                <td>{this.props.description}</td>
                <td><a href="#" onClick={this.handleClick}>Remove</a></td>
            </tr>
        )
    }
}
