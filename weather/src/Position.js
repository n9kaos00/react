import React from 'react';
import Weather from './Weather';
import './Position.css';

export default class Position extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            lat: 0,
            lng: 0,
            isLoaded: false
        };
    }

    componentDidMount() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                this.setState({
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                    isLoaded: true
                });
            });
        }
        else {
            alert('Your browser does not support geolocation!');
        }
    }

    render() {
        const { lat, lng, isLoaded } = this.state;
        if (!isLoaded) {
            return (
                <div className="container">
                    <p>Loading...</p>
                </div>
            )
        }
        else {
            return (
                <div className="container">
                    <h3>Your position</h3>
                    <p>Your position is {lat.toFixed(3)}, {lng.toFixed(3)}</p>
                    <Weather lat={lat} lng={lng} />
                </div>
            )
        }
    }
}
